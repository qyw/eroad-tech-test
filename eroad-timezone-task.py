#!/bin/python2

from __future__ import print_function
import sys
import csv
from datetime import datetime, timedelta #, strptime
#import pytz
import TimezoneId
from Point import Point

DEBUG = False #True
def nop(*args):
	return
debug_print = print if DEBUG else nop


class ERoadInput(object):
    def __init__(self, string_array):
    	self.utc_time_str = string_array[0]
        self.utc_time = datetime.strptime(string_array[0],"%Y-%m-%d %H:%M:%S")   # %H for 24 hours, %I for 12 hourse
        self.gps = Point(string_array[1],string_array[2])
        self.tzname = ''
        self.loc_time = datetime(2000,1,1)
        
    def __str__(self):
    	#return '{' + self.utc_time.strftime('%Y-%m-%d %H:%M:%S') + ', ' + str(self.gps) +'}'
    	return self.cvs_str()

    def cvs_str(self):
    	return self.utc_time_str +','+ self.gps.cvs_str() +','+ (self.tzname if self.tzname != 'UTC' else 'unknown(UTC)') +','+ self.loc_time.strftime("%Y-%m-%dT%H:%M:%S")

    def __repr__(self):
    	return ''


def read_input_file():
	er_inputs = []
	with open('input.csv', 'rb') as infile:
		reader = csv.reader(infile, delimiter=',')
		for row in reader:
			if row :
				time = row[0]
				latitude = float(row[1])
				longitude = float(row[2])
				er_inputs.append(ERoadInput(row))
				#print row
				debug_print( ERoadInput(row) )
	return er_inputs;


debug_print('Hello, ERoad!')
debug_print('Reading input file')
sys.stdout.flush()
er_inputs = read_input_file();
debug_print ('-----')

debug_print ('Loading database.')
sys.stdout.flush()
tizoid = TimezoneId.TimezoneId(sys.argv[1] if len(sys.argv)>1 else 'pytz')
debug_print ('tzwhere is ready')
debug_print ('-----')

debug_print('Looking for timezone.')
sys.stdout.flush()
with open("output.csv","w") as fout:
	for er in er_inputs:
		timezone = tizoid.TimezoneId(er.gps.X, er.gps.Y, er.utc_time)
		debug_print(timezone)
		#tzz = pytz.timezone(er.tzname)
		er.tzname = timezone['tzname']
		er.loc_time = timezone['loc_time'] #pytz.utc.localize(er.utc_time).astimezone(pytz.timezone(er.tzname))  ##er.utc_time.astimezone(tzz)  #
		#debug_print (er.loc_time.strftime('%Y-%m-%dT%H:%M:%S %Z%z'))
		outstr = er.cvs_str()
		print(outstr)
		sys.stdout.flush()
		fout.write(outstr+'\n')
