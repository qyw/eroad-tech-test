Hello ERoad.
------------

Task is done. Please test. Code is written in python2. It requires additional packages `tzwhere` and `pytz`.

    pip install tzwhere pytz

I had an idea to work with Google maps API. But I think local version is better in terms of independence from the Internet. However Google should give more accurate results, and it should not have "white spots". The underlying timezone data is based on work done by [Eric Muller](http://efele.net/maps/tz/world/).


#### Update.1

There was a problem with point (-44.490947,171.220966): `pytz` based on E.Muller's work doesn't know this point's timezone. But Google knows.  
So, I decided to spend another couple of hours to add Google.Maps as source. Now program requires `googlemaps` module

	pip install -U googlemaps

Run program with parameter:

    python eroad-timezone-task.py google
    python eroad-timezone-task.py pytz
    python eroad-timezone-task.py  # default is pytz


The task
--------
Develop a small application to read a CSV with a UTC datetime, latitude and longitude columns and append the timezone the vehicle is in and the localised datetime. See example of CSV input and output below. We will then run this over several test files with several rows of data. 

Input 
>2013-07-10 02:52:49,-44.490947,171.220966  
>2013-07-10 02:52:49,-33.912167,151.215820

Output 
>2013-07-10 02:52:49,-44.490947,171.220966,Pacific/Auckland,2013-07-10T14:52:49  
>2013-07-10 02:52:49,-33.912167,151.215820,Australia/Sydney,2013-07-10T12:52:49