
class Point(object):
	def __init__(self, x, y):
		self.X = float(x)
		self.Y = float(y)

	def distanceTo(self, other):
	    dx = self.X - other.X
	    dy = self.Y - other.Y
	    return math.hypot(dx, dy)

	def toTuple(self):
		return (self.X, self.Y)
	
	def tuple(self):
		return (self.X, self.Y)

	def __str__(self):
		return '{' + str(self.X) + ',' + str(self.Y) + '}'

	def cvs_str(self):
		return str(self.X) + ',' + str(self.Y)
