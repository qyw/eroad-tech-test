import pytz
from tzwhere import tzwhere
import googlemaps
#import datetime 
from datetime import datetime, timedelta


class TimezoneId:
	def __init__(self, source):
		if(source=='google'):
			self.gmaps = googlemaps.Client(key='AIzaSyBaG4qMq1N1HBryQdIhBn6iXDtpA2EFpXY')
		elif(source=='pytz'):
			self.tzwhere_ = tzwhere.tzwhere()
		else:
			raise ValueError('source expected to be google or pytz')
		self.source = source

	def TimezoneId(self, la, lo, utc_time):
		''' utc_time is datetime object '''
		if(self.source=='pytz'):
		 	tzname = self.tzwhere_.tzNameAt(la, lo)
			tzname = tzname if tzname else 'UTC'
			loc_time = pytz.utc.localize(utc_time).astimezone(pytz.timezone(tzname))
			return {'tzname': tzname, 'loc_time': loc_time}
		elif(self.source=='google'):
			tzdict = self.gmaps.timezone((la,lo))
			tzname = tzdict['timeZoneId']
			loc_time = utc_time - timedelta(seconds=tzdict['rawOffset'])
			#print utc_time, loc_time
			return {'tzname': tzname, 'loc_time': loc_time}

